/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var navigation_id = 0;
    $(".navigationArrow").last().addClass("fa-angle-down");
    $(".navigationArrowForm").last().addClass("fa-angle-down");
    $(".navigationVideos").last().addClass("fa-angle-down");
    $(".allNavigationVideos").last().addClass("fa-angle-down");
    $(".navigationArrowSolutionSection").last().addClass("fa-angle-down");
    //$(".navigationProductBannerArrow").last().addClass("fa-angle-down");
    $(".navigationBannerArrow").addClass("fa-angle-down");
    $('h5.quote_text').text('Load More..');
    $(".officeAddress").hide();
    $(".careers_form").hide();
    $(".videoSection").hide();
    $(".newVideoSection").hide();
    $(".solutionSectionClientNext").hide();
    $(".navigationArrow").click(function () {
        if (navigation_id === 1) {
            $(".officeAddress").hide("3000");
            $(this).addClass("fa-angle-down");
            $(this).removeClass("fa-angle-up");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load More...');
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".officeAddress").show("3000");
            $(this).addClass("fa-angle-up");
            $(this).removeClass("fa-angle-down");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load Less...');
            navigation_id = 1;
        }
    });
    $(".navigationArrowForm").click(function () {
        if (navigation_id === 1) {
            $(".careers_form").hide("3000");
            $(this).addClass("fa-angle-down");
            $(this).removeClass("fa-angle-up");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load More...');
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".careers_form").show("3000");
            $(this).addClass("fa-angle-up");
            $(this).removeClass("fa-angle-down");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load Less...');
                        /*$('html, body').animate({
                scrollTop: "+=500px"
            }, 1000);*/
            navigation_id = 1;
        }
    });
    $(".navigationVideos").click(function () {
        if (navigation_id === 1) {
            $(".videoSection").hide("3000");
            $(this).addClass("fa-angle-down");
            $(this).removeClass("fa-angle-up");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load More...');
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".videoSection").show("3000");
            $(this).addClass("fa-angle-up");
            $(this).removeClass("fa-angle-down");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load Less...');
            navigation_id = 1;
        }
    }); 
    $(".allNavigationVideos").click(function () {
        if (navigation_id === 1) {
            $(".newVideoSection").hide("3000");
            $(this).addClass("fa-angle-down");
            $(this).removeClass("fa-angle-up");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load More...');
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".newVideoSection").show("3000");
            $(this).addClass("fa-angle-up");
            $(this).removeClass("fa-angle-down");
            $(this).closest('.loadMoreSection').find('h5.quote_text').text('Load Less...');
            navigation_id = 1;
        }
    });    
    
    $(".navigationArrowSolutionSection").on('click', function () {
        var h5 = $(this).closest('.solutionSectionClient').find('h5.quote_text');
        console.log(h5.html())
        if (navigation_id === 1) {
            $(".solutionSectionClientNext").hide("3000");
            $(this).addClass("fa-angle-down");
            $(this).removeClass("fa-angle-up");
            h5.text('Load More..');
            navigation_id = 0;
        }else if (navigation_id === 0) {
            $(".solutionSectionClientNext").show("3000");
            $(this).addClass("fa-angle-up");
            $(this).removeClass("fa-angle-down");
            h5.text('Load Less..');
            navigation_id = 1;
        }
    });    

    $(".navigationProductBannerArrow").click(function () {
        $('html, body').animate({
        scrollTop: $(".bigVideoSection").offset().top
     }, 2000);
    });
	$(".navigationApplyButton").click(function () {
        $('html, body').animate({
        scrollTop: $(".services").offset().top
     }, 1000);
    });
    
    $('.input_style').hide();
	/*$('.btn-circle').on('click', function(e) {
            var windowSize = $( document ).width();
            if(windowSize >= '767'){
               $(".getInTouch").toggleClass('hidden');
            }
		e.preventDefault();
                 var toggleWidth = $(".input_style").width() == 150 ? "0px" : "150px";
		$('.input_style').animate({width: 'toggle'}).focus();
	});    */
        var getintouch = 1;
        $('.btn-circle').on('click', function(e) {
            var windowSize = $( document ).width();
           if(windowSize >= '767'){
               if(getintouch === 1){
                    $(".getInTouch").hide();
                    $(".search_box_style").show();
                    $(".search_box_style").css("width","138px");
                    getintouch = 0;
                }
                else if(getintouch === 0){
                    $(".getInTouch").show();
                    $(".search_box_style").hide();
                    getintouch = 1;
                }
            }
            else{
                  if(getintouch === 1){
                      $(".getInTouch").hide();
                    $(".search_box_style").show();
                    $(".search_box_style").css("width","114px");
                    getintouch = 0;
                }
                else if(getintouch === 0){
                    $(".getInTouch").show();
                    $(".search_box_style").hide();
                    getintouch = 1;
                }
            }
            
        });
     

    $(".navBSMaxMin").click(function () {
        if (navigation_id === 1) {
            $(".maxminBSIcon").addClass("fa-plus");
            $(".maxminBSIcon").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminBSIcon").addClass("fa-minus");
            $(".maxminBSIcon").removeClass("fa-plus");
            navigation_id = 1;
        }
    });  
    $(".navESMaxMin").click(function () {
        if (navigation_id === 1) {
            $(".maxminESIcon").addClass("fa-plus");
            $(".maxminESIcon").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminESIcon").addClass("fa-minus");
            $(".maxminESIcon").removeClass("fa-plus");
            navigation_id = 1;
        }
    });  
    $(".navCPMaxMin").click(function () {
        if (navigation_id === 1) {
            $(".maxminCPIcon").addClass("fa-plus");
            $(".maxminCPIcon").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminCPIcon").addClass("fa-minus");
            $(".maxminCPIcon").removeClass("fa-plus");
            navigation_id = 1;
        }
    });      
    $(".navAUMaxMin").click(function () {
        if (navigation_id === 1) {
            $(".maxminAUIcon").addClass("fa-plus");
            $(".maxminAUIcon").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminAUIcon").addClass("fa-minus");
            $(".maxminAUIcon").removeClass("fa-plus");
            navigation_id = 1;
        }
    });      
    
    /******** FOOTER ********/
        $(".navBSMaxMinFooter").click(function () {
        if (navigation_id === 1) {
            $(".maxminBSIconFooter").addClass("fa-plus");
            $(".maxminBSIconFooter").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminBSIconFooter").addClass("fa-minus");
            $(".maxminBSIconFooter").removeClass("fa-plus");
            navigation_id = 1;
        }
    });  
    $(".navESMaxMinFooter").click(function () {
        if (navigation_id === 1) {
            $(".maxminESIconFooter").addClass("fa-plus");
            $(".maxminESIconFooter").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminESIconFooter").addClass("fa-minus");
            $(".maxminESIconFooter").removeClass("fa-plus");
            navigation_id = 1;
        }
    });  
    $(".navCPMaxMinFooter").click(function () {
        if (navigation_id === 1) {
            $(".maxminCPIconFooter").addClass("fa-plus");
            $(".maxminCPIconFooter").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminCPIconFooter").addClass("fa-minus");
            $(".maxminCPIconFooter").removeClass("fa-plus");
            navigation_id = 1;
        }
    });      
    $(".navAUMaxMinFooter").click(function () {
        if (navigation_id === 1) {
            $(".maxminAUIconFooter").addClass("fa-plus");
            $(".maxminAUIconFooter").removeClass("fa-minus");
            navigation_id = 0;
        }
        else if (navigation_id === 0) {
            $(".maxminAUIconFooter").addClass("fa-minus");
            $(".maxminAUIconFooter").removeClass("fa-plus");
            navigation_id = 1;
        }
    });      
});
