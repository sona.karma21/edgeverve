/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
   
    $('.header2').css({'position': 'relative', 'display': 'none'});
    $(document).on('scroll', function () {
        var homeClassName = $('#home').attr('class');
        var menu1ClassName = $('#menu1').attr('class');
        var menu2ClassName = $('#menu2').attr('class');
        var menu3ClassName = $('#menu3').attr('class');
        var scrollTopHeight = 60;
        if ($("body").scrollTop() > scrollTopHeight) {
            $('.header2').css({'position': 'fixed', 'width': '100%', 'top': '0', 'z-index': '9999', 'display': 'block'});
        }
        else
        {
            $('.header2').css({'position': 'relative', 'display': 'none'});
        }
    });
});
