/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
                    $(document).ready(function () {
                        $(".videoImg").attr("src", "images/default_full_video.jpg");
                        $(".videoCaption").text("Lorem ipsum dolor sit amet Lorem ipsum dolor");
                        $(".videoDesc").text('"Union Bank of the Philippines, Philippines: Creating a culture and spirit of Innovation. Finacle gave the bank the ability to differentiate and respond at a customized level."');
                        $(".videoName").text("Justo Ortiz");
                        $(".videoDesig").text("Chairman and CEO");
                        $(".videoBank").text("UNION BANK");
                        $(".videos_wrapper").on("click", function () {
                            var videoImg = $(this).attr("data-video");
                            var videoCaption = $(this).attr("data-video-caption");
                            var videoDesc = $(this).attr("data-video-desc");
                            var videoName = $(this).attr("data-video-name");
                            var videoDesig = $(this).attr("data-video-desig");
                            var videoBank = $(this).attr("data-video-bank");
                            $(".videoImg").attr("src", videoImg);
                            $(".videoCaption").text(videoCaption);
                            $(".videoDesc").text(videoDesc);
                            $(".videoName").text(videoName);
                            $(".videoDesig").text(videoDesig);
                            $(".videoBank").text(videoBank);
                        });
                    });

